import 'bootstrap/dist/css/bootstrap.min.css';
import Header from "../src/components/Header/Header"
import Clients from './components/Clients/Clients';
import Service from './components/Service/Service';
import About from './components/About/About';
import './App.css';

function App() {
  return (
    <div className="App">
      <Header />
      <Clients />
      <Service />
      <About />
    </div>
  );
}

export default App;
