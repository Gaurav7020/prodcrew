import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import './clients.css'

export default function Clients() {
    return (
        <>
            <div className="client">
                <Container >
                    <div className="client_text">
                        <div>
                            <p>Happy Client</p>
                            <h2>6000+ Ratings Across Product Review</h2>
                            <span>customers feel good while interacting Right spanroduct will only be effective when customers feel good</span>
                            <Row className="client_count">
                                <Col>
                                <h1>2,500+</h1>
                                <p>Customers</p>
                                </Col>
                                <Col>
                                <h1>17M+</h1>
                                <p>Social Followers</p>
                                </Col>
                                <Col>
                                <h1>5,000+</h1>
                                <p>Monthl Blog Readers</p>
                                </Col>
                            </Row>
                        </div>
                    </div>
                </Container>
            </div>
        </>
    )
}
