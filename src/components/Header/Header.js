import React from 'react'
import './header.css'
import Link from "react-router-dom";
import { Navbar, Container, Nav, Button } from 'react-bootstrap';

export default function Header() {
  return (
    <>
    <Navbar collapseOnSelect expand="lg" className="Nav_bgcolor" variant="dark">
      <Container>
        <Navbar.Brand href="#home">Logo</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
          </Nav>
          <Nav>
          <Nav.Link href="#features">About</Nav.Link>
            <Nav.Link href="#pricing">Service</Nav.Link>
            <Nav.Link href="#features">Tech</Nav.Link>
            <Nav.Link href="#pricing">Our Engagement Model</Nav.Link>
            <Nav.Link href="#features">Blogs</Nav.Link>
            <Nav.Link href="#pricing">Career</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    <div  className="main">
      <Container >
        <div>
        <div className="main_text center_text">
          <h1 >Smart Product <br/> Development Partner</h1>
          <p>Right product will only be effective when customers feel good while interacting with the solution</p>
          <Button>Get In Touch</Button>
        </div>
        </div>
      </Container>
    </div>
    </>
  )
}
