
import React from 'react';
import { Container, Row, Col, Button, Image } from 'react-bootstrap';
import './service.css';

export default function Service() {
    return (
        <div>
            <Container>
                <div className="service_container">
                    <Row>
                        <Col sm={6}>
                        <h1>What is Product Market Fit ?</h1>
                        <p>"Product/market fit means being in a good market with a product that can satisfy that market and You can Always feel when product/market fit is happening"</p>
                        <br/>
                        <h5>By Marc Andreessen</h5>
                        <br/>
                        <Button>Learn More</Button>
                        </Col >
                        <Col sm={1}></Col>
                        <Col sm={4}>
                            <Image className="container service_img" src="/assests/images/service1.png"></Image>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    )
}
